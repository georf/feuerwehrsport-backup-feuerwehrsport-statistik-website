set :application, "feuerwehrsport-statistik"
set :user, "feuerwehrsport-statistik"
set :repository,  "git@github.com:Feuerwehrsport/feuerwehrsport-statistik-website.git"
set :required_packages, "make build-essential libreadline-dev git curl rsync postgresql imagemagick libpq-dev nodejs libyaml-dev libmagickwand-dev libgmp3-dev libmysqlclient-dev"
# libsqlite3-dev libmysqlclient-dev
set :rvm_ruby_string, '2.2.5'

# set :stages, %w(staging production)
# set :default_stage, "staging"
