source 'https://rubygems.org'

gem 'rails', '~> 4'
gem 'pg'

# Use SCSS for stylesheets
gem 'sass-rails', '~> 5'
gem 'jquery-rails'
gem 'bootstrap-sass'
#gem 'jquery-datatables-rails', '~> 3.3.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4'
# haml support
gem 'haml-rails'
gem 'simple_form'
gem 'cocoon' # nested_form helper
gem 'will_paginate'
gem 'will_paginate-bootstrap'

# Draper as model decorator
gem 'draper'
gem 'active_model_serializers'

# Use Unicorn as the app server
gem 'unicorn'
gem 'whenever', require: false
gem 'delayed_job_active_record'
gem 'daemons'

# image and pdf uploader
gem 'carrierwave'
gem 'rmagick'

# user rights management
gem 'devise'
gem 'devise-i18n-views'
gem 'cancancan'

# charts
gem 'lazy_high_charts'

# browser detection
gem 'browser'

# extra validations
gem 'validates_email_format_of'

# ics export
gem 'icalendar'

# export
gem 'axlsx_rails'
gem 'prawn'
gem 'prawn-table'
gem 'prawnto'
gem 'rqrcode_png'

# markdown
gem 'redcarpet'

# datetimepicker for simple_form
gem 'momentjs-rails', github: 'egeek/momentjs-rails'
# used for generating files:
# gem 'datetimepicker-rails', github: 'zpaulovics/datetimepicker-rails', branch: 'master', submodules: true

gem 'm3_log_file_parser', git: 'https://github.com/lichtbit/m3_log_file_parser.git'

group :development do
  gem 'm3_capistrano', require: false, git: 'ssh://git@cubie-rostock.chickenkiller.com:36000/m3_capistrano', branch: 'master', ref: 'b4f0dad9d6cd20eafa58e28b87e437265efe2514'
  # gem 'm3_capistrano', path: '../m3_capistrano'

  # to test email
  gem 'recipient_interceptor'
end

group :development, :test, :test_dump do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  #gem 'byebug'
  
  gem 'pry'
  gem 'pry-byebug'

  # Access an IRB console on exception pages or by using <%= console %> in views
  #gem 'web-console', '~> 2.0'
  gem 'timecop'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-commands-rspec'

  gem 'factory_girl'

  gem 'rspec-rails'
  gem 'rspec-collection_matchers'
  gem 'guard-rspec', require: false
  gem 'database_cleaner'
  gem 'capybara'
  gem 'poltergeist'
  gem 'capybara-screenshot'
end

