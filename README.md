# feuerwehrsport-statistik-website
Rails-Projekt der Webseite www.feuerwehrsport-statistik.de

[![Build Status](https://travis-ci.org/Feuerwehrsport/feuerwehrsport-statistik-website.svg?branch=master)](https://travis-ci.org/Feuerwehrsport/feuerwehrsport-statistik-website)

* [Derzeitige Webseite](https://www.feuerwehrsport-statistik.de/)
* [Alte PHP-Webseite](http://alt.feuerwehrsport-statistik.de/)
* [Entwicklerblog](http://www.georf.de/)
* Begin der Entwicklung: September 2015
* Erste Preview-Version: 25. Oktober 2015
* Umstellung auf Rails: 20. Januar 2016
* API für [Wettkampf-Manager](https://github.com/Feuerwehrsport/wettkampf-manager)

Weitere geplante Funktionen:
* Neue Wettkämpfe vom [Wettkampf-Manager](https://github.com/Feuerwehrsport/wettkampf-manager) importieren

Dieses Projekt ersetzt das vorherige [PHP-Projekt](https://github.com/georf/feuerwehrsport-statistik) zu der Webseite.
