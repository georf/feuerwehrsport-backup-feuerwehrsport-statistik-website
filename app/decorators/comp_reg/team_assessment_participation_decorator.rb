module CompReg
  class TeamAssessmentParticipationDecorator < ApplicationDecorator
    decorates_association :competition_assessment
  end
end